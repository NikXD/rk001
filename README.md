# rk001

*Хеш-функция*

Хеш-функия - функция, осуществляющая преобразование массива входных данных произвольной длины в (выходную) битовую
строку установленной длины, выполняемое определённым алгоритмом. Преобразование, производимое хеш-функцией, называется
хешированием. Исходные данные называются входным массивом, «ключом» или «сообщением». Результат преобразования (выходные
данные) называется «хешем», «хеш-кодом», «хеш-суммой», «сводкой сообщения»

Хеш-функции применяются в следующих случаях:

1)при построении ассоциативных массивов;

2)при поиске дубликатов в сериях наборов данных;

3)при построении уникальных идентификаторов для наборов данных;

4)при вычислении контрольных сумм от данных (сигнала) для последующего обнаружения в них ошибок (возникших случайно или
внесённых намеренно), возникающих при хранении и/или передаче данных;

5)при сохранении паролей в системах защиты в виде хеш-кода (для восстановления пароля по хеш-коду требуется функция,
являющаяся обратной по отношению к использованной хеш-функции);

6)при выработке электронной подписи (на практике часто подписывается не само сообщение, а его «хеш-образ»); и др

«Хорошая» хеш-функция должна удовлетворять двум свойствам:

быстрое вычисление; минимальное количество «коллизий». Введём обозначения:

_K_ — количество «ключей» (входных данных);
_h(k)_ — хеш-функция, имеющая не более {\displaystyle M}M различных значений (выходных данных). То есть:

_[0;\,K):h(k)<M}_ - В качестве примера «плохой» хеш-функции можно привести функцию с _M=1000}M=1000_, которая
десятизначному натуральному числу сопоставляет три цифры, выбранные из середины двадцатизначного квадрата числа _K_.
Казалось бы, значения «хеш-кодов» должны равномерно распределяться между «000» и «999», но для «реальных» данных это
справедливо лишь в том случае, если «ключи» не имеют «большого» количества нулей слева или справа.

Заключение:
------------------------------------
Хеш-функции преобразуют массив входных данных, произвольной длинны в битовою строку. Преобразовние этих строк и
называется *хеширование*.

1. Для чего используют криптографическая соль?

Криптографическая соль используется для защиты паролей и ЭЦП от подделки.

2. Для чего используют Криптографические хеш-функции?

Применяется в криптографической стойке, применяемых в криптографиях, и должна удовлетворять 3 основным критериям:

- Необратимость.

- Стойкость к коллизиям *первого* рода.

- Стойкость к коллизиям *второго* рода.

3. Каким свойствам должны удовлетворять хеш-функции?

Быстрое вычесление и минимальное колличество "коллизии"

4. Что такое хеш-функция?

Функция преобразующая массив входных данных, в битовую строку.

5. Как используется метод геометрическое хеширование?

Это метод применяемый в компьютерной графике, для решения задач на плоскости или в трехмерном пространстве.

6. Как применяется ускорение поиска данных?

Хеш-таблицей называют структуру данных, которые хранят в себе пары вида = "ключ" - "Хеш-код".
